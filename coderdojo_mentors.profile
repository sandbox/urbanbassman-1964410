<?php

/**
 * @file
 * Installation profile for the CoderDojo Mentors distribution.
 */

/**
 * Implements hook_modules_installed().
 *
 * When a module is installed, enable the modules it recommends if they are
 * present. For CoderDojo Mentors, also install permissions.
 */
/**
 * Check that other install profiles are not present to ensure we don't collide with a
 * similar form alter in their profile.
 *
 * Set CoderDojo Mentors as default install profile.
 */
if (!function_exists('system_form_install_select_profile_form_alter')) {
  function system_form_install_select_profile_form_alter(&$form, $form_state) {
    // Only set the value if CoderDojo Mentors is the only profile.
    if (count($form['profile']) == 1) {
      foreach($form['profile'] as $key => $element) {
        $form['profile'][$key]['#value'] = 'coderdojo_mentors';
      }
    }
  }
}
